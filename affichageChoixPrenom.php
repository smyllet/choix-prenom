<html>
<head>
    <meta charset="UTF-8">
    <title>Choix d'un prénom</title>
    <link rel="stylesheet" href="affichageChoixPrenom.css" />
</head>
<body>
    <h1>Choix d'un prénom</h1>
    <form action="affichageChoixPrenom.php" method="post">
        <label>Première lettre</label>
        <input type="text" name="firstLetter" size="1" maxlength="1" value="<?php if($_POST["firstLetter"]) echo $_POST["firstLetter"]?>">
        <br>
        <label>Longueur du prénom</label>
        <input type="number" name="size" min="1" size="2" max="20" value="<?php if($_POST["size"]) echo $_POST["size"]?>">
        <br>
        <input type="submit">
    </form>

    <hr>

    <?php
        include "choixPrenom.php";
        if($_POST)
        {
            if($_POST["firstLetter"] and $_POST["size"])
            {
                $prenoms = listPrenom($_POST["size"],$_POST["firstLetter"]);
                foreach ($prenoms as $prenom)
                {
                    echo $prenom."<br>";
                }
            }
        }
    ?>
</body>
</html>
<?php
    //- - - Fonction - - -//
    function listPrenom($size, $firstLetter)
    {
        $prenoms = []; //Tableau des prénoms retenus
        $file = fopen("prenoms.txt", "r"); //Ouverture du fichier prenom.txt
        if ($file) //Si il y a un fichier alors
        {
            while (($line = fgets($file)) !== false) //Tan que la ligne lu exist
            {
                $line = str_replace("\n", "", $line); //Suprimmer les sauts de lignes
                $line = str_replace("\r", "", $line);
                if(($line[0] == $firstLetter) && (strlen($line) == $size)) array_push($prenoms, $line); //Si le prénom commence par la lettre demandé et qu'il est de la longueur demandé alors on l'ajoute au tableau des prénoms
            }

            fclose($file); //Fermeture du fichier
        }
        else //sinon
        {
            echo "error"; //Afficher qu'il y a une erreur
        }

        return $prenoms; //Retour du tableau de prénom
    }

    //- - - Test de la fonction - - -//
    /*$prenoms = listPrenom(5, "b"); //Appel de la fonction pour rechercher des prénoms de 5 caractère commençant par la lettre b
    foreach ($prenoms as $prenom) //Pour chaque prénom contenu dans le tableau
    {
        echo $prenom."<br>"; //Afficher le prénom
    }*/
?>